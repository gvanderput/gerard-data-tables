import Row from "./Row";
import { renderHeaderText } from "./utils/renderHeaderText";
import { renderHeatmapValue } from "./utils/renderHeatmapValue";

export default function SalesTable({ employees, dates }: EmployeesAndDates) {
  return (
    <div>
      <Row
        title="Sales January"
        data={dates}
        renderValue={renderHeaderText}
        isHeader
      />
      {employees.map((employee) => {
        return (
          <Row
            key={employee.name}
            title={employee.name}
            data={employee.sales.map((sale) => sale.amount)}
            renderValue={renderHeatmapValue}
          />
        );
      })}
    </div>
  );
}
