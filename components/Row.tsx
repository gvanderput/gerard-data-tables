import styles from "../styles/Row.module.scss";
import classnames from "classnames";

export type Styles = typeof styles;

type RowProps = {
  title?: string;
  data: Value[];
  isHeader?: boolean;
  renderValue?: (value: Value, styles: Styles) => React.ReactNode;
};

export default function Row({ title, data, isHeader, renderValue }: RowProps) {
  return (
    <div className={classnames(styles.row, { [styles.header]: isHeader })}>
      <div className={styles.cell}>{title}</div>
      {data.map((value, index) => (
        <div key={`cell_${index}`} className={styles.cell}>
          {renderValue ? renderValue(value, styles) : value}
        </div>
      ))}
    </div>
  );
}
