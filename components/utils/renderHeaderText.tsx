import { Styles } from "../Row";
import { DateTime } from "luxon";

export function renderHeaderText(
  value: Value,
  styles: Styles
): React.ReactNode {
  const dt = DateTime.fromISO(value);
  return (
    <span className={styles.value}>
      {dt.toFormat("dd")}
      <br />
      <span>{dt.toFormat("MMM")}</span>
    </span>
  );
}
