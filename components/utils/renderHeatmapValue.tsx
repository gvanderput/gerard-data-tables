import { Styles } from "./Row";
import classnames from "classnames";

const ranges: { [key: string]: number } = {
  medium: 250,
  high: 375,
};

export function renderHeatmapValue(
  value: Value,
  styles: Styles
): React.ReactNode {
  return (
    <span
      className={classnames(styles.value, {
        [styles.medium]: value >= ranges.medium && value < ranges.high,
        [styles.high]: value >= ranges.high,
      })}
    >
      {value}
    </span>
  );
}
