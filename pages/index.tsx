import SalesTable from "../components/SalesTable";
import { getEmployeeSales, getSaleDates } from "../data/utils";
import styles from "../styles/Index.module.scss";

export default function Home(props: EmployeesAndDates) {
  return (
    <div className={styles.content}>
      <SalesTable {...props} />
    </div>
  );
}

export async function getStaticProps() {
  return {
    props: { employees: getEmployeeSales(), dates: getSaleDates() },
  };
}
